import React from 'react';
import './App.css';
// import Page from './components/Page';
// import Navbar from './components/Navbar';
// import Footer from './components/Footer';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


export default function App() {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <div className="collapse navbar-collapse container" id="navbarsExampleDefault">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link text-white" to="/">Home</Link>
              </li>
            </ul>
          </div>
        </nav>
        
        <footer className="footer">
          <div className="container">
            <span className="text-muted">@Afif Firdaus</span>
          </div>
        </footer>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/">
            {/* <Page /> */}
          </Route>
        </Switch>
      </div>
    </Router>
  );
};
